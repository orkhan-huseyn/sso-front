import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import Login from 'views/Login/loadable';
import Logout from 'views/Logout/loadable';
import Account from 'views/Account/loadable';
import NotFound from 'views/NotFound/loadable';
import Registration from 'views/Registration/loadable';
import ForgotPassword from 'views/ForgotPassword/loadable';
import PasswordReset from 'views/PasswordReset/loadable';

import { getJwtToken, isTokenExpired } from 'lib/util/jwt';

function App() {
  const isTokenValid = getJwtToken() && !isTokenExpired();

  return isTokenValid ? (
    <Switch>
      <Route exact path="/" render={() => <Redirect to="/account" />} />
      <Route path="/account" component={Account} />
      <Route path="/logout" component={Logout} />
      <Route component={NotFound} />
    </Switch>
  ) : (
    <Switch>
      <Route path="/registration" component={Registration} />
      <Route path="/forgot-password" component={ForgotPassword} />
      <Route path="/reset-password/:id" component={PasswordReset} />
      <Route component={Login} />
    </Switch>
  );
}

export default App;
