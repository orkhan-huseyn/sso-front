import styled, { css } from 'styled-components';

const Button = styled.button`
  border-radius: 3px;
  border: none;
  font-family: inherit;
  font-size: inherit;
  font-weight: 500;
  background-color: #0081ff0f;
  color: #0081ff;
  padding: 1rem 1.5rem;

  &:hover:not(:disabled) {
    box-shadow: 0 2px 20px rgba(0, 129, 255, 0.2),
      0 2px 20px rgba(0, 129, 255, 0.15);
  }

  &:disabled {
    background-color: var(--darkBorderColor);
    color: var(--darkTextColor);
  }

  svg {
    margin-right: 0.5rem;
  }

  ${props =>
    props.floating &&
    css`
      height: 4.5rem;
      width: 4.5rem;
      padding: 0;
      border-radius: 50%;
      display: flex;
      align-items: center;

      svg {
        margin: 0 auto;
      }
    `};

  ${props =>
    props.primary &&
    css`
      background-color: #0081ff;
      color: #fff;
    `};
`;

export default Button;
