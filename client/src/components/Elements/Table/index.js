import styled from 'styled-components';

const Table = styled.table`
  background-color: var(--whiteBackgroundColor);
  box-shadow: 0 8px 16px rgba(169, 194, 209, 0.1),
    0 32px 32px rgba(169, 194, 209, 0.15);
  border-radius: 10px;
  width: 100%;
  border-collapse: collapse;

  th,
  td {
    text-align: left;
    padding: 2rem;
  }

  tr:not(:last-child) {
    border-bottom: 1px solid var(--darkBorderColor);
  }

  [data-theme='dark'] & {
    box-shadow: 0 8px 16px rgba(0, 0, 0, 0.1), 0 32px 32px rgba(0, 0, 0, 0.15);
  }
`;

export default Table;
