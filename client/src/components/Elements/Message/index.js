import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styled from 'styled-components';

const MessageSpan = styled.div`
  background-color: var(--whiteBackgroundColor);
  box-shadow: rgba(9, 30, 66, 0.28) 0px 4px 8px -2px,
    rgba(9, 30, 66, 0.3) 0px 0px 1px;
  border-radius: 3px;
  text-align: center;
  margin-bottom: 3rem;
  padding: 2rem;
  color: var(--textDarkColor);
  font-size: 1.5rem;
`;

function Message({ show = false, error = false, success = false, message }) {
  if (!show) return null;
  const iconType = error ? 'exclamation-triangle' : 'check-circle';
  const showIcon = error || success;
  return (
    <MessageSpan>
      {showIcon && (
        <FontAwesomeIcon
          style={{ marginRight: '1rem' }}
          icon={['fas', iconType]}
        />
      )}
      {message}
    </MessageSpan>
  );
}

export default Message;
