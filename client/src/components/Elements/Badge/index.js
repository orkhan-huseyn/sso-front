import styled, { css } from 'styled-components';

const Badge = styled.span`
  display: inline-block;
  color: #fff;
  background-color: #0081ff;
  padding: 5px 9px;
  border-radius: 5px;
  ${props =>
    props.small &&
    css`
      font-size: 1.2rem;
      padding: 3px 7px;
    `}
`;

export default Badge;
