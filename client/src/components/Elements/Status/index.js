import styled from 'styled-components';

const Status = styled.span`
  display: inline-block;
  position: relative;

  &::after {
    content: '';
    position: absolute;
    top: 6px;
    left: -10px;
    width: 6px;
    height: 6px;
    border-radius: 50%;
    background-color: #0081ff;
  }
`;

export default Status;
