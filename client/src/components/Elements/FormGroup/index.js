import styled from 'styled-components';

const FormGroup = styled.div`
  margin-bottom: 2rem;

  label,
  small,
  input {
    display: block;
    width: 100%;
  }

  small {
    color: #ff3d57;
  }

  label {
    margin-bottom: 0.8rem;
    font-weight: bold;

    .required {
      color: #ff3d57;
    }
  }

  input {
    background-color: var(--bodyBackgroundColor);
    color: var(--darkTextColor);
    border: 1px solid rgb(223, 225, 230);
    border-radius: 5px;
    padding: 1rem 2rem;
    margin-bottom: 0.5rem;
    font-size: inherit;

    &.has-error {
      border-color: #ff3d57 !important;
    }
  }

  .input-group {
    position: relative;

    .input-group-addon {
      position: absolute;
      left: 1.5rem;
      top: 1.5rem;

      &.addon--right {
        right: 1.5rem;
        top: 1.5rem;
        left: auto;
      }

      svg {
        path {
          fill: var(--textDarkColor);
        }
      }
    }
  }

  [data-theme='dark'] & {
    input {
      border: 1px solid rgb(223, 225, 230, 0.2);
    }
  }
`;

export default FormGroup;
