import styled from 'styled-components';

const COLORS = ['#fdbf5e', '#ff3d57', '#22cce2', '#09b66d'];

const randomNumber = Math.floor(Math.random() * COLORS.length);
const randomColor = COLORS[randomNumber];

const Avatar = styled.span`
  background-color: ${randomColor};
  display: inline-block;
  border-radius: 50%;
  height: 4rem;
  width: 4rem;
  line-height: 4rem;
  color: #fff;
  font-weight: bold;
  margin-right: 0.5rem;
`;

export default Avatar;
