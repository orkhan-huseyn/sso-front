import React, { useRef } from 'react';
import classNames from 'classnames';
import './styles.scss';

function ToggleSwitch({ id, label, labelSrOnly, checked, onChange, icons }) {
  const checkboxRef = useRef(null);

  function onSwitchToggle(event) {
    onChange(event.target.checked);
  }

  function onLabelKeyDown(event) {
    // since event.keyCode is deprecated
    // we need to used event.key readonly property
    // to ready which key has been pressed
    // space key is returned with ' ' string
    // older browsers may return 'Space' insdeat of empty string
    const isSpaceKey = event.key === ' ' || event.key === 'Space';

    if (isSpaceKey) {
      // --- EXPLANATION ---
      // checkbox elements by default require Space key
      // to trigger the change event; in our case, we need to trigger
      // change event on 'label' element when Space key is pressed
      // because we have hidden the real checkbox element
      const checkbox = checkboxRef.current;
      if (checkbox) {
        onChange(!checkbox.checked);
      }
    }
  }

  return (
    <label
      onKeyDown={onLabelKeyDown}
      className="toggle-switch"
      htmlFor={id}
      tabIndex={0}
    >
      {/* Every input element must have a associated label element;
			So wee make labels required but we show them to only screen readers
			when it should not be shown to users */}
      <span className={classNames('label', { 'sr-only': labelSrOnly })}>
        {label}
      </span>
      <input
        id={id}
        ref={checkboxRef}
        type="checkbox"
        checked={checked}
        onChange={onSwitchToggle}
        tabIndex={-1}
      />
      <span className="toggle-switch__switch">{icons}</span>
    </label>
  );
}

export default ToggleSwitch;
