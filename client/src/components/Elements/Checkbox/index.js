import React, { useRef } from 'react';
import classNames from 'classnames';
import './styles.scss';

function Checkbox({ id, checked, onChange, label, labelSrOnly, readOnly }) {
  const checkboxRef = useRef(null);

  function onCheckboxChange(event) {
    onChange && onChange(event.target.checked);
  }

  function onLabelKeyDown(event) {
    // since event.keyCode is deprecated
    // we need to used event.key readonly property
    // to ready which key has been pressed
    // space key is returned with ' ' string
    // older browsers may return 'Space' insdeat of empty string
    const isSpaceKey = event.key === ' ' || event.key === 'Space';

    if (isSpaceKey) {
      // --- EXPLANATION ---
      // checkbox elements by default require Space key
      // to trigger the change event; in our case, we need to trigger
      // change event on 'label' element when Space key is pressed
      // because we have hidden the real checkbox element
      const checkbox = checkboxRef.current;
      if (checkbox) {
        onChange && onChange(!checkbox.checked);
      }
    }
  }

  return (
    <label
      htmlFor={id}
      onKeyDown={onLabelKeyDown}
      className="checkbox"
      tabIndex={0}
    >
      <input
        type="checkbox"
        id={id}
        ref={checkboxRef}
        checked={checked}
        tabIndex={-1}
        onChange={onCheckboxChange}
        readOnly={readOnly}
      />
      <span>
        <svg viewBox="0 0 24 24">
          <polyline points="20 6 9 17 4 12" />
        </svg>
      </span>
      {/* Every input element must have a associated label element;
			So wee make labels required but we show them to only screen readers
			when it should not be shown to users */}
      <span className={classNames({ 'sr-only': labelSrOnly })}>{label}</span>
    </label>
  );
}

export default Checkbox;
