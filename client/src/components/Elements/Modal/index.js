import React, { useRef } from 'react';
import classNames from 'classnames';
import './styles.scss';

function Modal({
  open,
  title,
  children,
  onClose,
  closeOnOutsideClick,
  width = 50
}) {
  const modalContentRef = useRef(null);
  const modalProps = {
    className: classNames('modal', { 'modal--open': open })
  };

  if (closeOnOutsideClick) {
    // close modal on outside click
    function onModalClick(event) {
      const modalContent = modalContentRef.current;
      if (modalContent && !modalContent.contains(event.target)) {
        onClose();
      }
    }

    modalProps['onClick'] = onModalClick;
  }

  return (
    <div {...modalProps}>
      <div
        ref={modalContentRef}
        className="modal__content"
        style={{ width: `${width}%` }}
      >
        <div className="modal__heading">
          <h1>{title}</h1>
          <button onClick={onClose}>
            <svg width="10" height="10" viewBox="0 0 10 10">
              <g>
                <path d="M1.716.273C1.982.53 3.952 2.564 5 3.647 6.048 2.564 8.018.53 8.284.273c.366-.352 1.023-.376 1.412 0 .39.375.42.9 0 1.359L6.421 5l3.275 3.37c.42.459.39.982 0 1.356-.389.376-1.046.352-1.412 0C8.018 9.472 6.048 7.437 5 6.353 3.952 7.437 1.982 9.472 1.716 9.727c-.366.352-1.023.376-1.412 0-.39-.374-.42-.897 0-1.357l3.275-3.369-3.275-3.37c-.42-.459-.39-.983 0-1.358.389-.376 1.046-.352 1.412 0z" />
              </g>
            </svg>
          </button>
        </div>
        <div className="modal__body">{children}</div>
      </div>
    </div>
  );
}

export default Modal;
