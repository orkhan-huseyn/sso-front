import React from 'react';
import { NavLink } from 'react-router-dom';
import './styles.scss';

function Navbar({ navbarItems }) {
	return (
		<section className="navbar">
			<h1>Hesab və tənzimləmələr</h1>
			<nav className="navbar__nav">
				{navbarItems.map((navbarItem) => (
					<NavLink key={navbarItem.path} to={navbarItem.path}>
						<span className="icon">
							<navbarItem.icon />
						</span>
						{navbarItem.title}
					</NavLink>
				))}
			</nav>
		</section>
	);
}

export default Navbar;
