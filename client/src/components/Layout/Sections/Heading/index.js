import React from 'react';

function HeadingSection({ pageTitle }) {
  return (
    <div className="page__heading">
      <h1>{pageTitle} </h1>
    </div>
  );
}

export default HeadingSection;
