import React from 'react';

function PageContent({ children }) {
  return <div className="page__content">{children}</div>;
}

export default PageContent;
