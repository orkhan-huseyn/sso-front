import React from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

function BreadCrump({ breadCrumpItems }) {
	return (
		<div className="page__breadcrump">
			<ul>
				<li>
					<Link title="Əsas səhifə" to="/">
						<FontAwesomeIcon icon={[ 'fas', 'home' ]} />
					</Link>
				</li>
				{breadCrumpItems.map(
					(breadCrumpItem, index) =>
						index === breadCrumpItems.length - 1 ? (
							<li key={index}>{breadCrumpItem.title}</li>
						) : (
							<li key={index}>
								<Link title={breadCrumpItem.title} to={breadCrumpItem.path}>
									{breadCrumpItem.title}
								</Link>
							</li>
						)
				)}
			</ul>
		</div>
	);
}

export default BreadCrump;
