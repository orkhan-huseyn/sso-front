import React from 'react';
import classNames from 'classnames';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { AppTheme } from 'lib/util/theme';
import { getUserFullName, getUserShortName } from 'lib/util/tokenData';
import { Switch as ToggleSwitch, Avatar } from 'components/Elements';

import logo from 'assets/logo.svg';
import './styles.scss';

const ToggleSwitchIcons = (
  <React.Fragment>
    <FontAwesomeIcon
      style={{ position: 'absolute', top: '8px', right: '5px' }}
      icon={['fas', 'moon']}
    />
    <FontAwesomeIcon
      style={{ position: 'absolute', top: '7px', left: '7px' }}
      icon={['fas', 'sun']}
    />
  </React.Fragment>
);

function Header({
  theme,
  changeTheme,
  isDropdownOpen,
  toggleDropdown,
  dropdownRef
}) {
  return (
    <header className="header">
      <div className="header__logo">
        <img src={logo} alt="logo" />
      </div>
      <div className="header__dark-mode">
        <ToggleSwitch
          id="darkModeSwitch"
          checked={theme === AppTheme.DARK}
          label="Gecə rejimi"
          labelSrOnly
          onChange={changeTheme}
          icons={ToggleSwitchIcons}
        />
      </div>
      {/* <div className="header__notifications">
        <button title="Bildirişlər">
          <FontAwesomeIcon icon={['far', 'bell']} />
        </button>
      </div> */}
      <div className="header__user">
        <div className="dropdown" ref={dropdownRef}>
          <button title="Çıxış və tənzimləmələr" onClick={toggleDropdown}>
            <Avatar>{getUserShortName()}</Avatar>
            {getUserFullName()}
            <span className="icon">
              <FontAwesomeIcon icon={['fas', 'caret-down']} />
            </span>
          </button>
          <div
            className={classNames('dropdown__content', {
              'dropdown__content--open': isDropdownOpen
            })}
          >
            <ul>
              <li>
                <Link to="/account/profile">
                  <FontAwesomeIcon icon={['fas', 'user-circle']} />
                  Şəxsi kabinet
                </Link>
              </li>
              <li>
                <Link to="/logout">
                  <FontAwesomeIcon icon={['fas', 'sign-out-alt']} />
                  Çıxış
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </header>
  );
}

export default Header;
