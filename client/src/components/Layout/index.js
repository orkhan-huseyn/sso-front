import React, { useState, useRef, useEffect } from 'react';
import { fromEvent } from 'rxjs';
import './styles.scss';
import {
  getCurrentTheme,
  setAppTheme,
  hydrateCurrentTheme,
  getNextTheme
} from 'lib/util/theme';

import Header from './Header';
import Navbar from './Navbar';
import SkipLink from '../SkipLink';
import PageContent from './Sections/Content';
import PageHeading from './Sections/Heading';
import PageBreadCrump from './Sections/BreadCrump';

import { HistoryIcon, UserIcon, ListIcon } from 'lib/icons/svg';

const navbarItems = [
  { title: 'Şəxsi məlumatlar', path: '/account/profile', icon: UserIcon },
  {
    title: 'Login tarixçəsi',
    path: '/account/login-history',
    icon: HistoryIcon
  },
  {
    title: 'Aktiv sessiyalar',
    path: '/account/active-sessions',
    icon: ListIcon
  }
];

function Layout({ children }) {
  const currentTheme = getCurrentTheme();
  hydrateCurrentTheme();

  const dropdownRef = useRef(null);
  const [theme, setTheme] = useState(currentTheme);
  const [dropdownOpen, setDropdownOpen] = useState(false);

  function changeAppTheme() {
    const nextTheme = getNextTheme();
    setTheme(nextTheme);
    setAppTheme(nextTheme);
  }

  function toggleDropdown() {
    setDropdownOpen(!dropdownOpen);
  }

  const outsideClick = fromEvent(document, 'click', {
    capture: true
  }).subscribe(event => {
    const dropdown = dropdownRef.current;
    if (dropdown && !dropdown.contains(event.target)) {
      setDropdownOpen(false);
    }
  });

  useEffect(() => {
    return function cleanup() {
      outsideClick.unsubscribe();
    };
  });

  return (
    <React.Fragment>
      <SkipLink />
      <Header
        dropdownRef={dropdownRef}
        isDropdownOpen={dropdownOpen}
        toggleDropdown={toggleDropdown}
        theme={theme}
        changeTheme={changeAppTheme}
      />
      <Navbar navbarItems={navbarItems} />
      <main id="main-content" className="page">
        {children}
      </main>
    </React.Fragment>
  );
}

Layout.Heading = PageHeading;
Layout.BreadCrump = PageBreadCrump;
Layout.Content = PageContent;

export default Layout;
