import React from 'react';
import './styles.scss';

function SkipLink() {
  return (
    <div id="skip-link">
      <a
        href="#main-content"
        title="Əsas məzuna keçid"
        className="element-invisible element-focusable"
      >
        Əsas məzmuna keçid
      </a>
    </div>
  );
}

export default SkipLink;
