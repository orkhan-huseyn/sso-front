const apiConfig = {
  development: {
    baseUrl: 'http://192.168.35.57:8080/api/v1/'
  },
  production: {
    baseUrl: 'http://192.168.35.57:8080/api/v1/'
  }
};

export default apiConfig[process.env.NODE_ENV];
