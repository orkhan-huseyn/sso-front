import React, { useState, useEffect, useRef } from 'react';
import logo from 'assets/asan_logo.png';
import './styles.scss';
import 'sass/_form.scss';

import API from 'api';
import { Title } from 'lib/util/view';
import { redirectToCallback } from 'lib/util/auth';
import { AUTH_MESSAGES } from 'lib/util/messages';
import LoginForm from './Form';

let signIn$, userNameCheck$;

function Login() {
  const [authMessage, setAuthMessage] = useState('');
  const [userImage, setUserImage] = useState(null);
  const [usernameChecked, setUsernameChecked] = useState(false);
  const [userFullName, setUserFullName] = useState('');
  const passwordInpRef = useRef(null);

  useEffect(() => {
    // let's keep this pleace
    // cleaner than we found it :)
    return () => {
      if (signIn$) signIn$.unsubscribe();
      if (userNameCheck$) userNameCheck$.unsubscribe();
    };
  }, []);

  function onFormSubmit(values, options) {
    if (!usernameChecked) {
      checkUserName(values, options);
    } else {
      signIn(values, options);
    }
  }

  function signIn(values, { setSubmitting }) {
    signIn$ = API.signIn({ ...values }).subscribe(({ response, status }) => {
      if (response && status === 200) {
        const accessToken = response.token;
        redirectToCallback(accessToken);
        setAuthMessage('');
      } else if (response && response.message) {
        const authError = AUTH_MESSAGES[response.message]
          ? AUTH_MESSAGES[response.message]
          : AUTH_MESSAGES.DEFAULT;
        setAuthMessage(authError);
      } else {
        setAuthMessage(AUTH_MESSAGES.CONNECTION_REFUSED);
      }
      setSubmitting(false);
    });
  }

  function checkUserName(values, { setSubmitting }) {
    userNameCheck$ = API.checkUserName(values).subscribe(
      ({ response, status }) => {
        if (response && status === 200) {
          setUserImage(`data:image/png;base64,${response.photo}`);
          setUsernameChecked(true);
          setUserFullName(
            `${response.lastName} ${response.firstName} ${response.patronymic}`
          );
          setAuthMessage('');
          // focust to password input
          // so that user can type without any distraction :)
          if (passwordInpRef.current) {
            passwordInpRef.current.focus();
          }
        } else if (response && response.message) {
          const authError = AUTH_MESSAGES[response.message]
            ? AUTH_MESSAGES[response.message]
            : AUTH_MESSAGES.DEFAULT;
          setAuthMessage(authError);
        } else {
          setAuthMessage(AUTH_MESSAGES.CONNECTION_REFUSED);
        }
        setSubmitting(false);
      }
    );
  }

  function editUsername() {
    setUsernameChecked(false);
    setUserImage('');
  }

  return (
    <div className="layout">
      {Title('Hesabınıza daxil olun')}

      <div className="login-container">
        <section className="avatar">
          <img
            src={userImage ? userImage : logo}
            alt="user avatar or asan logo"
          />
        </section>
        <section className="login-container__banner">
          <h1>{userFullName ? userFullName : 'Hesabınıza daxil olun'}</h1>
          <p>
            {usernameChecked
              ? 'Şifrənizi daxil etdikdən sonra "Daxil ol" düyməsinə basın'
              : 'İstifadəçi adınızı daxil etdikdən sonra "Davam et" düyməsinə basın'}
          </p>
        </section>
        <section className="login-container__form form">
          <LoginForm
            editUsername={editUsername}
            usernameChecked={usernameChecked}
            authMessage={authMessage}
            onFormSubmit={onFormSubmit}
            passwordInpRef={passwordInpRef}
          />
        </section>
      </div>
    </div>
  );
}

export default Login;
