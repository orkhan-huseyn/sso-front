import React from 'react';

const Login = React.lazy(() => import('./index'));

export default () => (
  <React.Suspense fallback={<div>Yüklənir.....</div>}>
    <Login />
  </React.Suspense>
);
