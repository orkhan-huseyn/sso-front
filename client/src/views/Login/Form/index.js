import React from 'react';
import { Formik } from 'formik';
import { Link } from 'react-router-dom';
import classNames from 'classnames';

import { UserIcon, PasswordIcon, EditIcon } from 'lib/icons/svg';
import { Message } from 'components/Elements';

function LoginForm({
  onFormSubmit,
  authMessage,
  usernameChecked,
  editUsername,
  passwordInpRef
}) {
  const initialValues = { username: '', password: '' };

  function validateFields(values) {
    let errors = {};
    if (!values.username) {
      errors.username = 'İstifadəçi adını daxil edin.';
    }
    if (!values.password && usernameChecked) {
      errors.password = 'Şifrənizi daxil edin.';
    }
    return errors;
  }

  return (
    <Formik
      initialValues={initialValues}
      validate={validateFields}
      onSubmit={onFormSubmit}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting
      }) => (
        <form onSubmit={handleSubmit} autoComplete="off">
          <Message show={!!authMessage} message={authMessage} error />
          <div className="form-group">
            <label className="sr-only" htmlFor="username">
              İstifadəçi adı
            </label>
            <div className="input-group">
              <span className="input-group-addon">
                <UserIcon />
              </span>
              <input
                id="username"
                name="username"
                type="text"
                className={classNames({
                  'has-error': errors.username && touched.username
                })}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.username}
                readOnly={usernameChecked}
                placeholder="İstifadəçi adını daxil edin"
                autoFocus
              />
              {usernameChecked && (
                <span
                  tabIndex={0}
                  title="Redaktə et"
                  onClick={editUsername}
                  className="input-group-addon addon--right"
                >
                  <EditIcon />
                </span>
              )}
              <small>
                {errors.username && touched.username && errors.username}
              </small>
            </div>
          </div>
          <div
            className={classNames('form-group', {
              'form-group--hidden': !usernameChecked
            })}
          >
            <label className="sr-only" htmlFor="password">
              Şifrəniz
            </label>
            <div className="input-group">
              <span className="input-group-addon">
                <PasswordIcon />
              </span>
              <input
                id="password"
                name="password"
                type="password"
                ref={passwordInpRef}
                className={classNames({
                  'has-error': errors.password && touched.password
                })}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
                placeholder="Şifrənizi daxil edin"
              />
              <small>
                {errors.password && touched.password && errors.password}
              </small>
            </div>
          </div>
          <div className="form-footer" style={{ marginTop: '0' }}>
            <button
              type="submit"
              title="Hesaba daxil ol"
              disabled={isSubmitting}
            >
              {isSubmitting
                ? 'Yoxlanılır...'
                : usernameChecked
                ? 'Daxil ol'
                : 'Davam et'}
            </button>
          </div>
          <div className="form-meta">
            <span>
              Hesabınız yoxdur?{' '}
              <Link title="Hesabın yaradılması" to="/registration">
                Qeydiyyatdan keçin
              </Link>
            </span>
            <Link title="Şifrənin bərpası" to="/forgot-password">
              Şifrəmi unutmuşam
            </Link>
          </div>
        </form>
      )}
    </Formik>
  );
}

export default LoginForm;
