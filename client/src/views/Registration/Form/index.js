import React from 'react';
import { Formik } from 'formik';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import classNames from 'classnames';

import { UserIcon, PasswordIcon, IDCardIcon } from 'lib/icons/svg';
import { Message } from 'components/Elements';

function RegistrationForm({
  onFormSubmit,
  authMessage,
  isPinValid,
  checkPin,
  checkingPin,
  setPinValid
}) {
  const initialValues = {
    pin: '',
    username: '',
    password: '',
    passwordConfirmation: ''
  };

  function validateFields(values) {
    let errors = {};

    if (!values.pin) {
      errors.pin = 'Şəxsiyyət vəsiqənizin FİN-ini daxil edin.';
    }

    if (values.pin.length !== 7) {
      errors.pin =
        'Şəxsiyyət vəsiqəsinin FİN-ini 7 simvoldan ibarət olmalıdır.';
    }

    if (!values.username) {
      errors.username = 'İstifadəçi adını daxil edin.';
    }
    if (!values.passwordConfirmation) {
      errors.passwordConfirmation = 'Şifrənin təkrarını daxil edin.';
    }
    if (!values.password) {
      errors.password = 'Şifrənizi daxil edin.';
    }

    if (values.password !== values.passwordConfirmation) {
      errors.passwordConfirmation =
        'Daxil etdiyiniz şifrələr bir biri ilə uyğun deyil.';
    }
    return errors;
  }

  return (
    <Formik
      initialValues={initialValues}
      validate={validateFields}
      onSubmit={onFormSubmit}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting
      }) => (
        <form onSubmit={handleSubmit} autoComplete="off">
          <Message
            show={!!authMessage.content}
            message={authMessage.content}
            error={authMessage.isError}
            success={authMessage.isSuccess}
          />
          <div className="form-group">
            <label className="sr-only" htmlFor="pin">
              Şəxsiyyət vəsiqəsinin FİN'i
            </label>
            <div className="input-group">
              <span className="input-group-addon">
                <IDCardIcon />
              </span>
              <input
                id="pin"
                name="pin"
                type="text"
                className={classNames({
                  'has-error': errors.pin && touched.pin
                })}
                onChange={handleChange}
                onBlur={event => {
                  handleBlur(event);
                  // check user's pin with HR system
                  // if he/she is not in HR system, he/she is not our employee
                  // thus, he/she cannot be reigstered
                  if (values.pin && !errors.pin) {
                    checkPin(values.pin);
                  } else {
                    setPinValid(false);
                  }
                }}
                value={values.pin}
                placeholder="Şəxsiyyət vəsiqənizin FİN-ini daxil edin"
                autoFocus
              />
              {checkingPin && (
                <span className="input-group-addon addon--right">
                  <FontAwesomeIcon icon={['fas', 'spinner']} spin />
                </span>
              )}
              {isPinValid && (
                <span
                  className="input-group-addon addon--right"
                  title="Əməkdaş İRQİS-də mövcuddur"
                >
                  <FontAwesomeIcon icon={['fas', 'check-circle']} />
                </span>
              )}
              <small>{errors.pin && touched.pin && errors.pin}</small>
            </div>
          </div>
          <div className="form-group">
            <label className="sr-only" htmlFor="username">
              İstifadəçi adı
            </label>
            <div className="input-group">
              <span className="input-group-addon">
                <UserIcon />
              </span>
              <input
                id="username"
                name="username"
                type="text"
                className={classNames({
                  'has-error': errors.username && touched.username
                })}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.username}
                placeholder="İstifadəçi adınızı daxil edin"
              />
              <small>
                {errors.username && touched.username && errors.username}
              </small>
            </div>
          </div>
          <div className="form-group">
            <label className="sr-only" htmlFor="password">
              Şifrəniz
            </label>
            <div className="input-group">
              <span className="input-group-addon">
                <PasswordIcon />
              </span>
              <input
                id="password"
                name="password"
                type="password"
                className={classNames({
                  'has-error': errors.password && touched.password
                })}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
                placeholder="Şifrəniz"
              />
              <small>
                {errors.password && touched.password && errors.password}
              </small>
            </div>
          </div>
          <div className="form-group">
            <label className="sr-only" htmlFor="passwordConfirmation">
              Şifrənin təkrarı
            </label>
            <div className="input-group">
              <span className="input-group-addon">
                <PasswordIcon />
              </span>
              <input
                id="passwordConfirmation"
                name="passwordConfirmation"
                type="password"
                className={classNames({
                  'has-error':
                    errors.passwordConfirmation && touched.passwordConfirmation
                })}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.passwordConfirmation}
                placeholder="Şifrənin təkrarı"
              />
              <small>
                {errors.passwordConfirmation &&
                  touched.passwordConfirmation &&
                  errors.passwordConfirmation}
              </small>
            </div>
          </div>
          <div className="form-footer" style={{ marginTop: '0' }}>
            <button
              type="submit"
              title="Hesaba daxil ol"
              disabled={isSubmitting || !isPinValid}
            >
              {isSubmitting ? 'Yoxlanılır...' : 'Qeydiyyat keç'}
            </button>
            {!isPinValid && (
              <small style={{ marginTop: '10px', display: 'inline-block' }}>
                Şəxsiyyət vəsiqənizin FİN-i yoxlanıldıqdan sonra "Qeydiyyatdan
                keçin" düyməsi aktivləşəcək
              </small>
            )}
          </div>
          <div className="form-meta">
            <span>
              Hesabınız var?{' '}
              <Link title="Hesaba giriş" to="/login">
                Hesabınıza daxil olun
              </Link>
            </span>
            <Link title="Şifrənin bərpası" to="/forgot-password">
              Şifrəmi unutmuşam
            </Link>
          </div>
        </form>
      )}
    </Formik>
  );
}

export default RegistrationForm;
