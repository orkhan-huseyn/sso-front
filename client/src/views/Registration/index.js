import React, { useState, useEffect } from 'react';
import logo from 'assets/asan_logo.png';
import './styles.scss';
import 'sass/_form.scss';

import API from 'api';
import { Title } from 'lib/util/view';
import { AUTH_MESSAGES } from 'lib/util/messages';
import RegistrationForm from './Form';

function Registration() {
  const [authMessage, setAuthMessage] = useState({
    isError: false,
    isSuccess: false,
    content: ''
  });
  const [userId, setUserId] = useState('');
  const [isPinValid, setPinValid] = useState(false);
  const [checkingPin, setCheckingPin] = useState(false);

  let signUp$, checkPin$;

  useEffect(() => {
    return () => {
      if (signUp$) signUp$.unsubscribe();
      if (checkPin$) checkPin$.unsubscribe();
    };
  }, [signUp$, checkPin$]);

  function onFormSubmit(values, options) {
    signUp(values, options);
  }

  function signUp(values, { setSubmitting }) {
    const requestBody = {
      id: userId,
      username: values.username,
      password: values.password,
      pin: values.pin
    };

    signUp$ = API.signUp(requestBody).subscribe(({ response, status }) => {
      if (response && status === 200) {
        setAuthMessage({
          content:
            'Qeydiyyatınız uğurla tamamlanmışdır! Az sonra daxil olma səhifəsinə yönləndiriləcəksiniz.',
          isError: false,
          isSuccess: true
        });
        setTimeout(() => {
          window.location.href = '/';
        }, 3000);
      } else if (response && response.message) {
        const authError = AUTH_MESSAGES[response.message]
          ? AUTH_MESSAGES[response.message]
          : AUTH_MESSAGES.DEFAULT;
        setAuthMessage({ content: authError, isError: true, isSuccess: false });
      } else {
        setAuthMessage({
          content: AUTH_MESSAGES.CONNECTION_REFUSED,
          isSuccess: false,
          isError: true
        });
      }
      setSubmitting(false);
    });
  }

  // check pin from HR system
  // to see if provided pin corresponds
  // any employee in the organization
  function checkPin(pin) {
    setCheckingPin(true);

    const requestBody = {
      username: pin
    };

    checkPin$ = API.checkUserInHr(requestBody).subscribe(
      ({ response, status }) => {
        if (response && status === 200) {
          setPinValid(true);
          setAuthMessage({ isError: false, isSuccess: false, content: '' });
          setUserId(response.id);
        } else if (response && response.message) {
          const authError = AUTH_MESSAGES[response.message]
            ? AUTH_MESSAGES[response.message]
            : AUTH_MESSAGES.DEFAULT;
          setAuthMessage({
            content: authError,
            isError: true,
            isSuccess: false
          });
        } else {
          setAuthMessage({
            content: AUTH_MESSAGES.CONNECTION_REFUSED,
            isSuccess: false,
            isError: true
          });
        }
        setCheckingPin(false);
      }
    );
  }

  return (
    <div className="layout">
      {Title('Qeydiyyat')}
      <div className="login-container">
        <section className="avatar">
          <img src={logo} alt="asan logo" />
        </section>
        <section className="login-container__banner">
          <h1>Qeydiyyat</h1>
          <p>Qeydiyyatdan keçmək üçün aşağıda tələb olunan xanaları doldurun</p>
        </section>
        <section className="login-container__form form">
          <RegistrationForm
            setPinValid={setPinValid}
            isPinValid={isPinValid}
            checkingPin={checkingPin}
            checkPin={checkPin}
            authMessage={authMessage}
            onFormSubmit={onFormSubmit}
          />
        </section>
      </div>
    </div>
  );
}

export default Registration;
