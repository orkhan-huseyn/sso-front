import React from 'react';

const Registration = React.lazy(() => import('./index'));

export default () => (
  <React.Suspense fallback={<div>Yüklənir.....</div>}>
    <Registration />
  </React.Suspense>
);
