import React from 'react';

const NotFound = React.lazy(() => import('./index'));

export default () => (
  <React.Suspense fallback={<div>Yüklənir.....</div>}>
    <NotFound />
  </React.Suspense>
);
