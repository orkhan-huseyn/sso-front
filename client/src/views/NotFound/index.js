import React from 'react';
import { withRouter } from 'react-router';
import './styles.scss';

import { Title } from '../../lib/util/view';
import { Button } from '../../components/Elements';

function NotFound({ history }) {
  function getHomePage() {
    history.push('/');
  }

  return (
    <div className="not-found">
      {Title('Səhifə Tapılmadı')}
      <div className="not-found__bg">
        <div className="container" style={{ position: 'relative', zIndex: 12 }}>
          <section className="not-found__error-code">
            <h1>404</h1>
          </section>
          <section className="not-found__error-message">
            <h2>Görünür siz yolunuzu çaşmısınız</h2>
          </section>
          <section className="not-found__error-description">
            <p>
              Axtardığınız səhifə mövcud deyil və ya başqa bir yerə köçürülüb
            </p>
            <Button
              title="Əsas səhifəyə qayıt"
              onClick={getHomePage}
              primary
              role="link"
              style={{ borderRadius: '5px' }}
            >
              Əsas səhifəyə qayıt
            </Button>
          </section>
        </div>
      </div>
    </div>
  );
}

export default withRouter(NotFound);
