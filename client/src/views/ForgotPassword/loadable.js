import React from 'react';

const ForgotPassword = React.lazy(() => import('./index'));

export default () => (
  <React.Suspense fallback={<div>Yüklənir.....</div>}>
    <ForgotPassword />
  </React.Suspense>
);
