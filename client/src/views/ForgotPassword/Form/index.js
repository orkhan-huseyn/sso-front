import React from 'react';
import { Formik } from 'formik';
import { Link } from 'react-router-dom';
import classNames from 'classnames';

import { UserIcon } from 'lib/icons/svg';
import { Message } from 'components/Elements';

function ForgotPasswordForm({ onFormSubmit, authMessage }) {
  const initialValues = { username: '' };

  function validateFields(values) {
    let errors = {};
    if (!values.username) {
      errors.username = 'İstifadəçi adını daxil edin.';
    }
    return errors;
  }

  return (
    <Formik
      initialValues={initialValues}
      validate={validateFields}
      onSubmit={onFormSubmit}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting
      }) => (
        <form onSubmit={handleSubmit} autoComplete="off">
          <Message
            show={!!authMessage.content}
            message={authMessage.content}
            error={authMessage.isError}
            success={authMessage.isSuccess}
          />
          <div className="form-group">
            <label className="sr-only" htmlFor="username">
              İstifadəçi adı
            </label>
            <div className="input-group">
              <span className="input-group-addon">
                <UserIcon />
              </span>
              <input
                id="username"
                name="username"
                type="text"
                className={classNames({
                  'has-error': errors.username && touched.username
                })}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.username}
                placeholder="İstifadəçi adını daxil edin"
                autoFocus
              />
              <small>
                {errors.username && touched.username && errors.username}
              </small>
            </div>
          </div>
          <div className="form-footer">
            <button
              type="submit"
              title="Hesaba daxil ol"
              disabled={isSubmitting}
            >
              {isSubmitting ? 'Yoxlanılır...' : 'Göndər'}
            </button>
          </div>
          <div className="form-meta">
            <span>
              Hesabınız yoxdur?{' '}
              <Link title="Hesabın yaradılması" to="/registration">
                Qeydiyyatdan keçin
              </Link>
            </span>
            <Link title="Hesaba daxil ol" to="/">
              Hesabınız daxil olun
            </Link>
          </div>
        </form>
      )}
    </Formik>
  );
}

export default ForgotPasswordForm;
