import React, { useState } from 'react';
import './styles.scss';
import 'sass/_form.scss';

import API from 'api';
import { Title } from 'lib/util/view';
import { AUTH_MESSAGES } from 'lib/util/messages';
import ForgotPasswordForm from './Form';

function ForgotPassword() {
  const [authMessage, setAuthMessage] = useState({
    isError: false,
    isSuccess: false,
    content: ''
  });

  function onFormSubmit(values, options) {
    checkUserName(values, options);
  }

  function checkUserName(values, { setSubmitting }) {
    const requestBody = {
      username: values.username
    };
    API.forgotPassword(requestBody).subscribe(({ response, status }) => {
      if (status === 200) {
        setAuthMessage({
          content:
            'Şifrənin bərpası üçün sizə e-poçt göndərildi. Zəhmət olmasa, e-poçt qutunuzu yoxlayın.',
          isError: false,
          isSuccess: true
        });
      } else if (response && response.message) {
        const authError = AUTH_MESSAGES[response.message]
          ? AUTH_MESSAGES[response.message]
          : AUTH_MESSAGES.DEFAULT;
        setAuthMessage({ content: authError, isError: true, isSuccess: false });
      } else {
        setAuthMessage({
          content: AUTH_MESSAGES.CONNECTION_REFUSED,
          isError: true,
          isSuccess: false
        });
      }
      setSubmitting(false);
    });
  }

  return (
    <div className="layout">
      {Title('Şifrənin bərpası')}
      <div className="fp-container">
        <section className="fp-container__banner">
          <h1>Şifrənin bərpası</h1>
          <p>
            İstifadəçi adınızı daxil etdikdən sonra, əgər bu həqiqətən
            sizsinizsə, biz sizə şifrənizin bərpası ilə bağlı e-poçt
            göndərəcəyik
          </p>
        </section>
        <section className="fp-container__form form">
          <ForgotPasswordForm
            authMessage={authMessage}
            onFormSubmit={onFormSubmit}
          />
        </section>
      </div>
    </div>
  );
}

export default ForgotPassword;
