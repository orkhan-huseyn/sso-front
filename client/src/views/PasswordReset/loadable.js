import React from 'react';

const PasswordReset = React.lazy(() => import('./index'));

export default () => (
  <React.Suspense fallback={<div>Yüklənir.....</div>}>
    <PasswordReset />
  </React.Suspense>
);
