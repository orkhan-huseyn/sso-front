import React from 'react';
import { Formik } from 'formik';
import { Link } from 'react-router-dom';
import classNames from 'classnames';

import { PasswordIcon } from 'lib/icons/svg';
import { Message } from 'components/Elements';

function ResetPasswordForm({ onFormSubmit, authMessage }) {
  const initialValues = { password: '', passwordConfirmation: '' };

  function validateFields(values) {
    let errors = {};
    if (!values.passwordConfirmation) {
      errors.passwordConfirmation = 'Şifrənin təkrarını daxil edin.';
    }
    if (!values.password) {
      errors.password = 'Şifrənizi daxil edin.';
    }

    if (values.password !== values.passwordConfirmation) {
      errors.passwordConfirmation =
        'Daxil etdiyiniz şifrələr bir biri ilə uyğun deyil.';
    }

    return errors;
  }

  return (
    <Formik
      initialValues={initialValues}
      validate={validateFields}
      onSubmit={onFormSubmit}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting
      }) => (
        <form onSubmit={handleSubmit} autoComplete="off">
          <Message
            show={!!authMessage.content}
            message={authMessage.content}
            error={authMessage.isError}
            success={authMessage.isSuccess}
          />
          <div className="form-group">
            <label className="sr-only" htmlFor="password">
              Şifrəniz
            </label>
            <div className="input-group">
              <span className="input-group-addon">
                <PasswordIcon />
              </span>
              <input
                id="password"
                name="password"
                type="password"
                className={classNames({
                  'has-error': errors.password && touched.password
                })}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
                placeholder="Yeni şifrə"
                autoFocus
              />
              <small>
                {errors.password && touched.password && errors.password}
              </small>
            </div>
          </div>
          <div className="form-group">
            <label className="sr-only" htmlFor="passwordConfirmation">
              Şifrənin təkrarı
            </label>
            <div className="input-group">
              <span className="input-group-addon">
                <PasswordIcon />
              </span>
              <input
                id="passwordConfirmation"
                name="passwordConfirmation"
                type="password"
                className={classNames({
                  'has-error':
                    errors.passwordConfirmation && touched.passwordConfirmation
                })}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.passwordConfirmation}
                placeholder="Yeni şifrənin təkrarı"
              />
              <small>
                {errors.passwordConfirmation &&
                  touched.passwordConfirmation &&
                  errors.passwordConfirmation}
              </small>
            </div>
          </div>
          <div className="form-footer">
            <button
              type="submit"
              title="Hesaba daxil ol"
              disabled={isSubmitting}
            >
              {isSubmitting ? 'Şifrə dəyişdirilir...' : 'Şifrəni dəyiş'}
            </button>
          </div>
          <div className="form-meta">
            <span>
              Hesabınız yoxdur?{' '}
              <Link title="Hesabın yaradılması" to="/registration">
                Qeydiyyatdan keçin
              </Link>
            </span>
            <Link title="Hesaba daxil ol" to="/">
              Hesaba daxil ol
            </Link>
          </div>
        </form>
      )}
    </Formik>
  );
}

export default ResetPasswordForm;
