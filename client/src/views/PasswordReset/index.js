import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import './styles.scss';
import 'sass/_form.scss';

import API from 'api';
import { Title } from 'lib/util/view';
import { AUTH_MESSAGES } from 'lib/util/messages';
import ResetPasswordForm from './Form';

function PasswordReset({ match, history }) {
  let checkHash$, passwordReset$;
  const [authMessage, setAuthMessage] = useState({
    isError: false,
    isSuccess: false,
    content: ''
  });

  useEffect(() => {
    checkPasswordResetId();
    return function cleanup() {
      if (checkHash$) checkHash$.unsubscribe();
      if (passwordReset$) passwordReset$.unsubscribe();
    };
  });

  function onFormSubmit(values, { setSubmitting }) {
    const requestBody = {
      passwordResetId: match.params.id,
      ...values
    };
    passwordReset$ = API.resetPassword(requestBody).subscribe(
      ({ response, status }) => {
        if (status === 200) {
          setAuthMessage({
            content:
              'Şifrəniz yeniləndi! Az sonra hesaba daxil olma səhifəsinə yönləndiriləcəksiniz...',
            isError: false,
            isSuccess: true
          });
          setTimeout(() => {
            history.push('/');
          }, 2000);
        } else if (response && response.message) {
          const authError = AUTH_MESSAGES[response.message]
            ? AUTH_MESSAGES[response.message]
            : AUTH_MESSAGES.DEFAULT;
          setAuthMessage({
            content: authError,
            isError: true,
            isSuccess: false
          });
        } else {
          setAuthMessage({
            content: AUTH_MESSAGES.CONNECTION_REFUSED,
            isSuccess: false,
            isError: true
          });
        }
        setSubmitting(false);
      }
    );
  }

  function checkPasswordResetId() {
    const requestBody = {
      passwordResetId: match.params.id
    };

    checkHash$ = API.checkHash(requestBody).subscribe(
      ({ response, status }) => {
        if (response && status === 200) {
          setAuthMessage('');
        } else if (response && response.message) {
          const authError = AUTH_MESSAGES[response.message]
            ? AUTH_MESSAGES[response.message]
            : AUTH_MESSAGES.DEFAULT;
          setAuthMessage(authError);
        } else if (status === 0) {
          setAuthMessage(AUTH_MESSAGES.CONNECTION_REFUSED);
        }
      }
    );
  }

  return (
    <div className="layout">
      {Title('Yeni şifrənin təyin edilməsi')}
      <div className="login-container">
        <section className="login-container__banner">
          <h1>Yeni şifrənin təyin edilməsi</h1>
          <p>
            Yeni şifrənizi və şifrənin təkrarını daxil etdikdən sonra "Şifrəni
            dəyiş" düyməsinə basın
          </p>
        </section>
        <section className="login-container__form form">
          <ResetPasswordForm
            authMessage={authMessage}
            onFormSubmit={onFormSubmit}
          />
        </section>
      </div>
    </div>
  );
}

export default withRouter(PasswordReset);
