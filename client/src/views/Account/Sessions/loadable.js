import React from 'react';

const Sessions = React.lazy(() => import('./index'));

export default () => (
  <React.Suspense fallback={<div>Yüklənir.....</div>}>
    <Sessions />
  </React.Suspense>
);
