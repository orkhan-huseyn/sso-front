import React, { useState, useEffect, useCallback } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './styles.scss';

import API from 'api';
import { BreadCrump, Heading, Content } from 'components/Layout/Sections';
import { Badge, Button } from 'components/Elements';
import { checkSessionExpiry } from 'lib/util/requestUtils';
import { toLocaleDateFormat } from 'lib/util/dateTime';

const breadCrumpItems = [
  { title: 'Hesab və tənzimləmələr', path: '/account' },
  { title: 'Aktiv sessiyalar', path: 'account/active-sessions' }
];

let fetchSessions$;

function ActiveSessions() {
  const [sessions, setSessions] = useState([]);
  const [isFetching, setIsFetching] = useState(false);

  const memoizedFetchSessions = useCallback(() => {
    setIsFetching(true);
    fetchSessions$ = API.getActiveSessions().subscribe(
      ({ response, status }) => {
        checkSessionExpiry({ response, status });
        if (status === 200) {
          const sessionList = response.map(s => ({
            ...s,
            isClosing: false
          }));
          setSessions(sessionList);
          setIsFetching(false);
        }
      }
    );
  }, []);

  useEffect(() => {
    memoizedFetchSessions();
    return () => fetchSessions$ && fetchSessions$.unsubscribe();
  }, [memoizedFetchSessions]);

  function endSession(sessionId) {
    const updatedSessionList = sessions.map(s => ({
      ...s,
      isClosing: s.id === sessionId
    }));
    setSessions(updatedSessionList);
    API.endSession(sessionId).subscribe(({ response, status }) => {
      checkSessionExpiry({ response, status });
      const updatedSessionList = sessions.map(s => ({
        ...s,
        isClosing: false
      }));
      setSessions(updatedSessionList);
      if (status === 200) {
        memoizedFetchSessions();
      }
    });
  }

  return (
    <React.Fragment>
      <Heading pageTitle="Login tarixçəsi" />
      <BreadCrump breadCrumpItems={breadCrumpItems} />
      <Content>
        <div className="login-history">
          <table className="login-history__list">
            <thead>
              <tr>
                <th>Əməliyyat sistemi</th>
                <th>Brauzer</th>
                <th>Sessiyanın yaradılma vaxtı</th>
                <th>Bitmə vaxtı</th>
                <th>İP ünvanı</th>
                <th>Əməliyyatlar</th>
              </tr>
            </thead>
            <tbody>
              {isFetching && (
                <tr>
                  <td style={{ textAlign: 'center' }} colSpan={6}>
                    <strong>Yüklənir...</strong>
                  </td>
                </tr>
              )}
              {!isFetching &&
                sessions.map(session => (
                  <tr key={session.id}>
                    <td>
                      <FontAwesomeIcon
                        icon={['fab', session.os.toLowerCase()]}
                      />
                      {session.os}
                      {session.currentSession && (
                        <Badge small style={{ marginLeft: '1rem' }}>
                          Cari sessiya
                        </Badge>
                      )}
                    </td>
                    <td>
                      <FontAwesomeIcon
                        icon={['fab', session.browser.toLowerCase()]}
                      />
                      {session.browser}
                    </td>
                    <td>
                      <FontAwesomeIcon icon={['far', 'clock']} />
                      {toLocaleDateFormat(session.dateCreated)}
                    </td>
                    <td>
                      <FontAwesomeIcon icon={['far', 'clock']} />
                      {toLocaleDateFormat(session.expireDate)}
                    </td>
                    <td>
                      <strong>{session.ip}</strong>
                    </td>
                    <td>
                      <Button
                        onClick={() => endSession(session.id)}
                        disabled={session.currentSession || session.isClosing}
                      >
                        {session.isClosing
                          ? 'Bağlanılır...'
                          : 'Sessiyanı bitir'}
                      </Button>
                    </td>
                  </tr>
                ))}
            </tbody>
          </table>
        </div>
      </Content>
    </React.Fragment>
  );
}

export default ActiveSessions;
