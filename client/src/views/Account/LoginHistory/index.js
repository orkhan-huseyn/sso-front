import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import ReactPaginate from 'react-paginate';
import './styles.scss';

import API from 'api';
import { BreadCrump, Heading, Content } from 'components/Layout/Sections';
import { toLocaleDateFormat } from 'lib/util/dateTime';
import { checkSessionExpiry } from 'lib/util/requestUtils';

const breadCrumpItems = [
  { title: 'Hesab və tənzimləmələr', path: '/account' },
  { title: 'Login tarixçəsi', path: 'account/login-history' }
];

function LoginHistory() {
  const limit = 10;
  const [isFetching, setIsFetching] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const [history, setHistory] = useState([]);
  const [pageCount, setPageCount] = useState(0);

  useEffect(() => {
    setIsFetching(true);
    const fetchLoginHistory$ = API.getLoginHistory(
      currentPage,
      limit
    ).subscribe(({ response, status }) => {
      setIsFetching(false);
      if (status === 200) {
        checkSessionExpiry({ response, status });
        setHistory(response.list);
        setPageCount(Math.ceil(response.totalCount / limit));
      }
    });
    return () => fetchLoginHistory$.unsubscribe();
  }, [currentPage]);

  function onPageChange(data) {
    setCurrentPage(data.selected);
  }

  return (
    <React.Fragment>
      <Heading pageTitle="Login tarixçəsi" />
      <BreadCrump breadCrumpItems={breadCrumpItems} />
      <Content>
        <div className="login-history">
          <table className="login-history__list">
            <thead>
              <tr>
                <th>#</th>
                <th>Əməliyyat sistemi</th>
                <th>Brauzer</th>
                <th>Sessiyanın yaranma vaxtı</th>
                <th>Bitmə vaxtı</th>
                <th>İP ünvanı</th>
              </tr>
            </thead>
            <tbody>
              {!isFetching && !history.length && (
                <tr>
                  <td style={{ textAlign: 'center' }} colSpan={6}>
                    <strong>Tarixçə boşdur</strong>
                  </td>
                </tr>
              )}
              {isFetching && (
                <tr>
                  <td style={{ textAlign: 'center' }} colSpan={6}>
                    <strong>Yüklənir...</strong>
                  </td>
                </tr>
              )}
              {!isFetching &&
                history.map((historyItem, index) => (
                  <tr key={historyItem.id}>
                    <td>{currentPage * limit + index + 1}</td>
                    <td>
                      <FontAwesomeIcon
                        icon={['fab', historyItem.os.toLowerCase()]}
                      />
                      {historyItem.os}
                    </td>
                    <td>
                      <FontAwesomeIcon
                        icon={['fab', historyItem.browser.toLowerCase()]}
                      />
                      {historyItem.browser}
                    </td>
                    <td>
                      <FontAwesomeIcon icon={['far', 'clock']} />
                      {toLocaleDateFormat(historyItem.dateCreated)}
                    </td>
                    <td>
                      <FontAwesomeIcon icon={['far', 'clock']} />
                      {toLocaleDateFormat(historyItem.expireDate)}
                    </td>
                    <td>
                      <strong>{historyItem.ip}</strong>
                    </td>
                  </tr>
                ))}
            </tbody>
          </table>
        </div>
        <div className="login-history__pagination">
          {pageCount > 1 ? (
            <ReactPaginate
              previousLabel="Əvvəlki"
              nextLabel="Növbəti"
              breakLabel="..."
              breakClassName="break"
              pageCount={pageCount}
              marginPagesDisplayed={2}
              pageRangeDisplayed={5}
              onPageChange={onPageChange}
              containerClassName="pagination"
              subContainerClassName="pages pagination"
              activeClassName="active"
            />
          ) : null}
        </div>
      </Content>
    </React.Fragment>
  );
}

export default LoginHistory;
