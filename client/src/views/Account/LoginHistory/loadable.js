import React from 'react';

const LoginHistory = React.lazy(() => import('./index'));

export default () => (
  <React.Suspense fallback={<div>Yüklənir.....</div>}>
    <LoginHistory />
  </React.Suspense>
);
