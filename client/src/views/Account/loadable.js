import React from 'react';

const Account = React.lazy(() => import('./index'));

export default () => (
  <React.Suspense fallback={<div>Yüklənir.....</div>}>
    <Account />
  </React.Suspense>
);
