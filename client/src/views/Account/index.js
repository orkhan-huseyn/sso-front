import React from 'react';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';

import Layout from 'components/Layout';

import Sessions from './Sessions';
import LoginHistory from './LoginHistory';
import ProfileSettings from './ProfileSettings';

function Account({ match }) {
  return (
    <Layout>
      <Switch>
        <Route
          exact
          path={`${match.path}/`}
          component={() => <Redirect to="/account/profile" />}
        />
        <Route path={`${match.path}/login-history`} component={LoginHistory} />
        <Route path={`${match.path}/active-sessions`} component={Sessions} />
        <Route path={`${match.path}/profile`} component={ProfileSettings} />
      </Switch>
    </Layout>
  );
}

export default withRouter(Account);
