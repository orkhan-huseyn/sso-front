import React from 'react';

import { BreadCrump, Heading, Content } from 'components/Layout/Sections';

import ProfileInfo from './ProfileInfo';
import PasswordChange from './PasswordChange';
import Applications from './Applications';

const breadCrumpItems = [
  { title: 'Hesab və tənzimləmələr', path: '/account' },
  { title: 'Şəxsi məlumatlar', path: 'account/profile' }
];

function ProfileSettings() {
  return (
    <React.Fragment>
      <Heading pageTitle="Şəxsi məlumatlar" />
      <BreadCrump breadCrumpItems={breadCrumpItems} />
      <Content>
        <div
          style={{
            display: 'flex',
            alignItems: 'flex-start'
          }}
        >
          <ProfileInfo />
          <Applications />
          <PasswordChange />
        </div>
      </Content>
    </React.Fragment>
  );
}

export default ProfileSettings;
