import React, { useState } from 'react';
import classNames from 'classnames';
import { Formik } from 'formik';

import API from 'api';
import { FormGroup, Button, Message } from 'components/Elements';
import { checkSessionExpiry } from 'lib/util/requestUtils';
import { AUTH_MESSAGES } from 'lib/util/messages';
import deviceInfo from 'lib/util/device';

import { getInitialValues, validateFields } from './formUtils';
import './styles.scss';

function PasswordChange() {
  const initialValues = getInitialValues();
  const [authMessage, setAuthMessage] = useState({
    content: '',
    isError: false,
    isSuccess: false
  });

  function onFormSubmit(values, { setSubmitting }) {
    const requestBody = {
      oldPassword: values.oldPassword,
      newPassword1: values.newPassword,
      newPassword2: values.newPasswordConfirmation,
      os: deviceInfo.os,
      browser: deviceInfo.browser
    };

    API.changePassword(requestBody).subscribe(({ response, status }) => {
      checkSessionExpiry({ response, status });
      if (status === 200) {
        setAuthMessage({
          content:
            'Şifrəniz yeniləndi! Əgər hesabınızdan kiminsə istifadə etdiyini düşünürsünüzsə, aktiv sessiyalar bölməsinə girib, bütün sessiyaları sonlandıra bilərsiniz.',
          isError: false,
          isSuccess: true
        });
      } else if (response && response.message) {
        const authError = AUTH_MESSAGES[response.message]
          ? AUTH_MESSAGES[response.message]
          : AUTH_MESSAGES.DEFAULT;
        setAuthMessage({ content: authError, isError: true, isSuccess: false });
      } else {
        setAuthMessage({
          content: AUTH_MESSAGES.CONNECTION_REFUSED,
          isError: true,
          isSuccess: false
        });
      }
      setSubmitting(false);
    });
  }

  return (
    <div className="password-change">
      <h2 style={{ marginBottom: '3rem' }}>Şifrənin dəyişdirilməsi</h2>
      <Formik
        initialValues={initialValues}
        validate={validateFields}
        onSubmit={onFormSubmit}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting
        }) => (
          <form onSubmit={handleSubmit} autoComplete="off" className="form">
            <Message
              show={!!authMessage.content}
              message={authMessage.content}
              error={authMessage.isError}
              success={authMessage.isSuccess}
            />
            <FormGroup>
              <label htmlFor="oldPassword">
                Cari şifrəniz: <span className="required">*</span>
              </label>
              <input
                id="oldPassword"
                type="password"
                name="oldPassword"
                className={classNames({
                  'has-error': errors.oldPassword && touched.oldPassword
                })}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.oldPassword}
                placeholder="Cari şifrənizi daxil edin"
              />
              <small>
                {errors.oldPassword &&
                  touched.oldPassword &&
                  errors.oldPassword}
              </small>
            </FormGroup>
            <FormGroup>
              <label htmlFor="newPassword">
                Yeni şifrə: <span className="required">*</span>
              </label>
              <input
                id="newPassword"
                type="password"
                name="newPassword"
                className={classNames({
                  'has-error': errors.newPassword && touched.newPassword
                })}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.newPassword}
                placeholder="Yeni şifrəni daxil edin"
              />
              <small>
                {errors.newPassword &&
                  touched.newPassword &&
                  errors.newPassword}
              </small>
            </FormGroup>
            <FormGroup>
              <label htmlFor="newPasswordConfirmation">
                Cari şifrəniz: <span className="required">*</span>
              </label>
              <input
                id="newPasswordConfirmation"
                type="password"
                name="newPasswordConfirmation"
                className={classNames({
                  'has-error':
                    errors.newPasswordConfirmation &&
                    touched.newPasswordConfirmation
                })}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.newPasswordConfirmation}
                placeholder="Yeni şifrəni təkrar daxil edin"
              />
              <small>
                {errors.newPasswordConfirmation &&
                  touched.newPasswordConfirmation &&
                  errors.newPasswordConfirmation}
              </small>
            </FormGroup>
            <div style={{ marginTop: '3rem', textAlign: 'right' }}>
              <Button primary type="submit" disabled={isSubmitting}>
                {isSubmitting ? 'Göndərilir...' : 'Şifrəni dəyiş'}
              </Button>
            </div>
          </form>
        )}
      </Formik>
    </div>
  );
}

export default PasswordChange;
