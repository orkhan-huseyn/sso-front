export function getInitialValues() {
  return {
    oldPassword: '',
    newPassword: '',
    newPasswordConfirmation: ''
  };
}

export function validateFields(values) {
  let errors = {};
  if (!values.oldPassword) errors.oldPassword = 'Cari şifrənizi daxil edin';
  if (!values.newPassword) errors.newPassword = 'Yeni şifrəni daxil edin';
  if (!values.newPasswordConfirmation)
    errors.newPasswordConfirmation = 'Yeni şifrənin təkrarını daxil edin';
  if (values.newPassword !== values.newPasswordConfirmation)
    errors.newPasswordConfirmation =
      'Yazığınız şifrələr bir-biri ilə uyğun deyil';
  return errors;
}
