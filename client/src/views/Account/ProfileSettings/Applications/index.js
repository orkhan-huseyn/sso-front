import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import API from 'api';
import { Badge } from 'components/Elements';

import './styles.scss';
import { checkSessionExpiry } from 'lib/util/requestUtils';

function Applications() {
  const [appList, setAppList] = useState([]);

  useEffect(() => {
    const getAppList$ = API.getPermittedApps().subscribe(
      ({ response, status }) => {
        checkSessionExpiry({ response, status });
        if (status === 200) {
          setAppList(
            response.list.map(item => ({
              ...item,
              domains: item.domains.split(',')
            }))
          );
        }
      }
    );

    return () => getAppList$.unsubscribe();
  }, []);

  return (
    <div className="apps">
      <h2>Sistemlər</h2>
      <ul className="apps__list">
        {appList.map(app => (
          <li key={app.label}>
            <span style={{ display: 'inline-block', marginBottom: '1rem' }}>
              <FontAwesomeIcon
                style={{ fontSize: '5rem' }}
                icon={['fas', app.logo || 'file']}
              />
            </span>
            <span>
              <a
                href={app.domains[0]}
                target="_blank"
                rel="noopener noreferrer"
              >
                <h3>{app.name}</h3>
              </a>
              <p>{app.description}</p>
              {app.roles.map(role => (
                <Badge key={role.id} style={{ marginRight: '3px' }} small>
                  {role.name}
                </Badge>
              ))}
            </span>
          </li>
        ))}
      </ul>
    </div>
  );
}

export default Applications;
