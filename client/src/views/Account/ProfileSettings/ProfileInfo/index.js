import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import API from 'api';
import { getUserFullName, getUsername } from 'lib/util/tokenData';
import { checkSessionExpiry } from 'lib/util/requestUtils';
import { placeholderImage } from 'lib/util/image';

import './styles.scss';

function ProfileInfo() {
  const [userInfo, setUserInfo] = useState({
    id: '',
    lastName: '',
    firstName: '',
    patronymic: '',
    photo: '',
    posName: '',
    strName: ''
  });

  useEffect(() => {
    let getCurrentUser$;

    const savedUserInfo = localStorage.getItem('userInfo');
    if (savedUserInfo) {
      setUserInfo(JSON.parse(savedUserInfo));
    } else {
      getCurrentUser$ = API.getCurrentUser().subscribe(
        ({ response, status }) => {
          checkSessionExpiry({ response, status });
          if (status === 200) {
            setUserInfo(response);
            localStorage.setItem('userInfo', JSON.stringify(response));
          }
        }
      );
    }

    return () => {
      if (getCurrentUser$) getCurrentUser$.unsubscribe();
    };
  }, []);

  return (
    <div className="profile">
      <section className="profile__general-info">
        <div className="avatar">
          <img
            src={`data:image/png;base64,${
              userInfo.photo ? userInfo.photo : placeholderImage
            }`}
            alt="user avatar"
          />
        </div>
        <div className="headline">
          <h2>{getUserFullName({ includeFatherName: true })}</h2>
          <p>
            {userInfo.strName}, {userInfo.posName}
          </p>
        </div>
      </section>
      <section className="profile__account-info">
        <ul>
          <li>
            <FontAwesomeIcon icon={['fa', 'id-card']} />
            <span title="İstifadəçi adı">{getUsername()}</span>
          </li>
          {/* <li>
            <FontAwesomeIcon icon={['fa', 'envelope']} />
            <span title="Korporativ e-poçt">o.huseynli@asan.gov.az</span>
          </li>
          <li>
            <FontAwesomeIcon icon={['fa', 'phone']} />
            <span title="Korporativ mobil telefon nömrəsi">
              +994 50 9789914
            </span>
          </li> */}
        </ul>
      </section>
    </div>
  );
}

export default ProfileInfo;
