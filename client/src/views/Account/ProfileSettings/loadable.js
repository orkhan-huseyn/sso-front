import React from 'react';

const ChangePassword = React.lazy(() => import('./index'));

export default () => (
  <React.Suspense fallback={<div>Yüklənir.....</div>}>
    <ChangePassword />
  </React.Suspense>
);
