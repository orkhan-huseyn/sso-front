import React from 'react';

const Logout = React.lazy(() => import('./index'));

export default () => (
  <React.Suspense fallback={<div>Yüklənir.....</div>}>
    <Logout />
  </React.Suspense>
);
