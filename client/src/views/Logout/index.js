import React, { useEffect } from 'react';
import { AUTH_MESSAGES } from 'lib/util/messages';
import { cleanupAndKickOut } from 'lib/util/auth';
import API from 'api';

function Logout() {
  useEffect(() => {
    API.logOut().subscribe(({ response, status }) => {
      // clear the storage and cookies after logout
      if (status === 200) {
        cleanupAndKickOut();
      } else if (response && response.message) {
        if (response.message === AUTH_MESSAGES.ALREADY_LOGOUTED) {
          cleanupAndKickOut();
        }
      }
    });
  }, []);

  return (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100vh'
      }}
    >
      <h1>Sistemdən çıxış edilir..</h1>
    </div>
  );
}

export default Logout;
