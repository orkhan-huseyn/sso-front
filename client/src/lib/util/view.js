import React from 'react';
import Helmet from 'react-helmet';

/**
 * Get page title with Helmet
 * @param {string} pageTitle
 * @returns {any}
 **/
export function Title(pageTitle) {
  return (
    <Helmet>
      <title>{pageTitle} | Tək Hesab - Bütün Sistemlər</title>
    </Helmet>
  );
}
