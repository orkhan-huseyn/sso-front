export const AUTH_MESSAGES = {
  INVALID_PASSWORD: 'Üzr istəyirik! Daxil etdiyiniz şifrə yanlışdır.',
  INVALID_USERNAME: 'Üzr istəyirik! Sistemdə belə istifadəçi yoxdur.',
  IP_BLOCKED:
    'Üzr istəyirik! Sizin İP ünvanınız çoxlu sayda yanlış istifadəçi adı və şifrə kombinasiyası daxil etdiyiniz üçün bloklanmışdır.',
  DEFAULT: 'Üzr istəyirik! Müraciət zamanı xəta baş verdi.',
  CONNECTION_REFUSED:
    'Üzr istəyirik! Müraciət edilən serverə qoşulmaq mümkün olmadı.',
  INVALID_HASH:
    'Generasiya edilmiş heş keçərli deyil. URL-in düzgün olduğundan və heç bir dəyişikliyin olmadığından əmin olun.',
  ALREADY_SENT:
    'Sizin e-poçtunuza şifrənin bərpası üçün keçid artıq göndərilmişdir. Zəhmət olmasa e-poçtunuzu yoxlayın.',
  PIN_NOT_IN_HR: 'Üzr istəyirik! İRQİS-də belə əməkdaş yoxdur.',
  ALREADY_REGISTERED:
    'Üzr istəyirik! Görünür, siz artıq qeydiyyatdan keçminiz. Əgər şifrənizi unutmusunuzsa, "Şifrəni unutmuşam" linkinə keçərək, şifrənizi bərpa edə bilərsiniz.',
  ALREADY_LOGOUTED: 'ALREADY_LOGOUTED'
};
