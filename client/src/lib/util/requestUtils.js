import deviceInfo from './device';
import { cleanupAndKickOut } from './auth';

export const isDevelopment = process.env.NODE_ENV === 'development';

export const defaultHeaders = {
  'Content-Type': 'application/json',
  Os: deviceInfo.os,
  Browser: deviceInfo.browser
};

export function checkSessionExpiry({ response, status }) {
  if (isDevelopment) return;
  if (status === 401 || status === 403) {
    cleanupAndKickOut();
  }
}
