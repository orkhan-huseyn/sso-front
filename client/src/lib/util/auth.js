import queryString from 'query-string';
import { COOKIE_KEY } from './jwt';
import cookie from 'react-cookies';

function getQueryParams() {
  const urlParams = queryString.parse(window.location.search);
  return urlParams;
}

export function redirectToCallback(accessToken) {
  const { callbackUrl, appId } = getQueryParams();
  // if callback url exists
  // it means login required from other domain
  // so we redirect back to there
  if (appId && callbackUrl) {
    const appDomain = atob(appId);
    const referrerUrl = `${appDomain}${callbackUrl}?accessToken=${accessToken}`;
    window.location.href = referrerUrl;
  } else {
    window.location.href = '/';
  }
}

export function cleanupAndKickOut() {
  localStorage.clear();
  cookie.remove(COOKIE_KEY, { path: '/' });
  setTimeout(function() {
    window.location.href = '/';
  }, 500);
}
