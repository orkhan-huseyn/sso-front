/**
 * Convert string date to Azerbaijan date format
 * @param  {string} dateString input date
 * @return {string} formatted date
 */
export function toLocaleDateFormat(dateString) {
  let date = new Date(dateString);
  let year = date.getUTCFullYear();

  let month = date.getUTCMonth() + 1;
  month = month > 10 ? month : `0${month}`;

  let day = date.getUTCDate();
  day = day > 10 ? day : `0${day}`;

  let hours = date.getUTCHours();
  hours = hours > 10 ? hours : `0${hours}`;

  let minutes = date.getUTCMinutes();
  minutes = minutes > 10 ? minutes : `0${minutes}`;

  return `${day}.${month}.${year} ${hours}:${minutes}`;
}
