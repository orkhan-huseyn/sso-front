import { getTokenContent } from './jwt';

/**
 * Returns user's full name
 * @param {{includeFatherName: boolean}} options
 * @returns {string}
 */
export function getUserFullName(options) {
  const tokenContent = getTokenContent();
  if (options && options.includeFatherName) {
    return `${tokenContent.lastName} ${tokenContent.firstName} ${tokenContent.patronymic}`;
  }
  return `${tokenContent.firstName} ${tokenContent.lastName}`;
}

/**
 * Get pin from user
 * @returns {string} user pin
 */
export function getUsername() {
  return getTokenContent().username;
}

/**
 * Returns first letter of user's first and last name
 * @returns {string}
 */
export function getUserShortName() {
  const tokenContent = getTokenContent();
  const firstLetterOfFirstName = tokenContent.firstName.substring(0, 1);
  const firstLetterOfLastName = tokenContent.lastName.substring(0, 1);
  return `${firstLetterOfFirstName}${firstLetterOfLastName}`;
}
