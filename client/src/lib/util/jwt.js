import jwt_decode from 'jwt-decode';
import cookie from 'react-cookies';
import { isDevelopment } from './requestUtils';

export const COOKIE_KEY = 'SID';

/**
 * Get jwt token from sotrage as string
 * @returns {string}
 */
export function getJwtToken() {
  if (isDevelopment) {
    const STATIC_TOKEN =
      'eyJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJPcnhhbiIsImxhc3ROYW1lIjoiSHVzZXlubGkiLCJwYXRyb255bWljIjoiUmF1ZiIsInBpbiI6IjY0U1lWVkIiLCJyb2xlcyI6W3siZGVzY3JpcHRpb24iOiJNyZlya8mZemkgxLBkZW50aWZpa2FzaXlhIHbJmSDEsGNhesmZbMmZciBTaXN0ZW1pIiwibGFiZWwiOiJST0xFX0FETUlOIiwibmFtZSI6ImFkbWluIiwicm9sZUlkIjoxLCJhcHBOYW1lIjoiQ2FhcyIsImFwcExhYmVsIjoiY2FhcyIsImFwcFVybCI6Imh0dHA6Ly9jYWFzLmFzYW4ub3JnIn0seyJkZXNjcmlwdGlvbiI6Ik3JmXJryZl6aSDEsGRlbnRpZmlrYXNpeWEgdsmZIMSwY2F6yZlsyZlyIFNpc3RlbWkiLCJyb2xlSWQiOjIsImFwcE5hbWUiOiJDYWFzIiwibGFiZWwiOiJERVZFTE9QRVIiLCJhcHBMYWJlbCI6ImNhYXMiLCJhcHBVcmwiOiJodHRwOi8vY2Fhcy5hc2FuLm9yZyIsIm5hbWUiOiJkZXYifSx7InJvbGVJZCI6MiwiZGVzY3JpcHRpb24iOiLEsG5zYW4gUmVzdXJzbGFyxLFuxLFuIFFleWRpeXlhdMSxIMSwbmZvcm1hc2l5YSBTaXN0ZW1pIiwiYXBwTmFtZSI6IsSwUlHEsFMiLCJsYWJlbCI6IkRFVkVMT1BFUiIsImFwcFVybCI6Imh0dHA6Ly9oci5hc2FuLm9yZyIsIm5hbWUiOiJkZXYiLCJhcHBMYWJlbCI6IkhSIn1dLCJpZCI6IjAyQTE4NTM3LUEyRDgtNDVCMC04NzIxLTI0MjA1OURBM0RBOSIsInVzZXJuYW1lIjoiNjRTWVZWQiIsInN1YiI6IjY0U1lWVkIiLCJpYXQiOjE1Nzg4OTU4NTksImV4cCI6MTU3OTUwMDY1OX0.h3Q5LkvSk7_nzAqqGglxSVn5tr7-hSBLj5W4ZizRqGv42goujQ2Nc6kouZU_4o1dx6MNlsE11O8alWVojP_sUA';
    cookie.save(COOKIE_KEY, STATIC_TOKEN, { path: '/' });
  }
  return cookie.load(COOKIE_KEY);
}

/**
 * Check is token is expired
 * @returns {boolean}
 */
export function isTokenExpired() {
  const tokenExpTime = getTokenExpTime();
  return new Date().getTime() >= tokenExpTime * 1000;
}

/**
 * Decodes jwt and returns its content
 * @returns {Object}
 */
export function getTokenContent() {
  const jwtToken = getJwtToken();
  if (jwtToken) {
    return jwt_decode(jwtToken);
  }
  return null;
}

/**
 * Gets expire time of jwt token
 * @returns {number}
 */
export function getTokenExpTime() {
  const tokenContent = getTokenContent();
  return tokenContent.exp;
}

/**
 * Returns true if token exists
 * @returns {boolean}
 */
export function tokenExists() {
  const token = getJwtToken();
  return typeof token !== 'undefined' && token !== null;
}
