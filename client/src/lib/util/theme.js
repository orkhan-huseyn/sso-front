export const AppTheme = {
  LIGHT: 'light',
  DARK: 'dark'
};

const THEME_HTML_ATTRIB = 'data-theme';
const THEME_STORAGE_KEY = 'app-theme';

/**
 * Returns current theme
 * @returns {AppTheme} current theme
 */
export function getCurrentTheme() {
  const currentTheme = localStorage.getItem(THEME_STORAGE_KEY);
  if (currentTheme) {
    return currentTheme;
  }
  return getOsTheme();
}

/**
 * Saves app theme in DOM and local storage
 * @param theme {AppTheme}
 * @returns {void}
 */
export function setAppTheme(theme) {
  document.documentElement.setAttribute(THEME_HTML_ATTRIB, theme);
  localStorage.setItem(THEME_STORAGE_KEY, theme);
}

/**
 * Sets current theme to the DOM
 * @returns {void}
 */
export function hydrateCurrentTheme() {
  const currentTheme = getCurrentTheme();
  setAppTheme(currentTheme);
}

/**
 * Decides what is gonna be next theme
 * @returns {AppTheme} next theme
 */
export function getNextTheme() {
  const currentTheme = getCurrentTheme();
  return currentTheme === AppTheme.LIGHT ? AppTheme.DARK : AppTheme.LIGHT;
}

/**
 * Return prefered OS Theme
 * @returns {AppTheme.DARK | AppTheme.LIGHT}
 */
export function getOsTheme() {
  // get theme of OS with CSS prefers-color-sheme query
  // if it matches dark mode, switch to dark mode
  // else switch to light mode
  const prefersDark = window.matchMedia('(prefers-color-scheme: dark)');
  return prefersDark.matches ? AppTheme.DARK : AppTheme.LIGHT;
}
