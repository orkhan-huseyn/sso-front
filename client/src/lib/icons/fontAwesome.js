import { library } from '@fortawesome/fontawesome-svg-core';
import { faBell, faClock } from '@fortawesome/free-regular-svg-icons';
import {
  faChrome,
  faFirefox,
  faOpera,
  faSafari,
  faLinux,
  faWindows
} from '@fortawesome/free-brands-svg-icons';
import {
  faHome,
  faCaretDown,
  faSun,
  faMoon,
  faSignOutAlt,
  faUserCircle,
  faSpinner,
  faCheckCircle,
  faEnvelope,
  faPhone,
  faIdCard,
  faExclamationTriangle,
  faUsers,
  faShieldAlt,
  faFile
} from '@fortawesome/free-solid-svg-icons';

library.add(
  // solid icons
  faHome,
  faCaretDown,
  faSun,
  faMoon,
  faSignOutAlt,
  faUserCircle,
  faSpinner,
  faCheckCircle,
  faEnvelope,
  faPhone,
  faIdCard,
  faExclamationTriangle,
  faUsers,
  faShieldAlt,
  faFile,
  // regular icons
  faBell,
  faClock,
  //brand icons
  faChrome,
  faFirefox,
  faOpera,
  faSafari,
  faLinux,
  faWindows
);
