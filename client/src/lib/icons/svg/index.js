export { default as UserIcon } from './user';
export { default as PasswordIcon } from './password';
export { default as EditIcon } from './edit';
export { default as SearchIcon } from './search';
export { default as HistoryIcon } from './history';
export { default as ListIcon } from './list';
export { default as IDCardIcon } from './idcard';
