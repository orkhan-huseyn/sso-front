import React from 'react';

function SearchIcon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="16"
      height="16"
      viewBox="0 0 16 16"
    >
      <g>
        <path d="M12.735 7.429a5.306 5.306 0 1 1-10.612 0 5.306 5.306 0 0 1 10.612 0zm-.903 6.02l2.217 2.217a1.143 1.143 0 0 0 1.616-1.617l-2.217-2.217-.02-.02a7.429 7.429 0 1 0-1.616 1.616l.02.02z" />
      </g>
    </svg>
  );
}

export default SearchIcon;
