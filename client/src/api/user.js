import { of } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { catchError } from 'rxjs/operators';
import { defaultHeaders } from 'lib/util/requestUtils';
import apiConfig from 'config/apiConfig';

export function getCurrentUser() {
  return ajax({
    method: 'GET',
    url: `${apiConfig.baseUrl}currentUser`,
    headers: defaultHeaders,
    withCredentials: true
  }).pipe(catchError(error => of(error)));
}

export function getPermittedApps() {
  return ajax({
    method: 'GET',
    url: `${apiConfig.baseUrl}permittedApps`,
    headers: defaultHeaders,
    withCredentials: true
  }).pipe(catchError(error => of(error)));
}

export function getActiveSessions() {
  return ajax({
    method: 'GET',
    url: `${apiConfig.baseUrl}activeSessions`,
    headers: defaultHeaders,
    withCredentials: true
  }).pipe(catchError(error => of(error)));
}

export function getLoginHistory(offset, max) {
  return ajax({
    method: 'GET',
    url: `${apiConfig.baseUrl}loginHistory?offset=${offset}&max=${max}`,
    headers: defaultHeaders,
    withCredentials: true
  }).pipe(catchError(error => of(error)));
}

export function endSession(sessionId) {
  return ajax({
    method: 'PUT',
    url: `${apiConfig.baseUrl}sessionKill/${sessionId}`,
    headers: defaultHeaders,
    withCredentials: true
  }).pipe(catchError(error => of(error)));
}
