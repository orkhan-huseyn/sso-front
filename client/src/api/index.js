import * as authenticationServices from './authentication';
import * as dashboardServices from './dashboard';
import * as userServices from './user';

const API = {
  ...authenticationServices,
  ...dashboardServices,
  ...userServices
};

export default API;
