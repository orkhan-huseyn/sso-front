import { of } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { catchError } from 'rxjs/operators';
import apiConfig from 'config/apiConfig';
import { defaultHeaders } from 'lib/util/requestUtils';

export function signIn(requestBody) {
  return ajax({
    method: 'POST',
    url: `${apiConfig.baseUrl}signIn`,
    body: requestBody,
    headers: defaultHeaders
  }).pipe(
    catchError(error => {
      return of(error);
    })
  );
}

export function signUp(requestBody) {
  return ajax({
    method: 'POST',
    url: `${apiConfig.baseUrl}signUp`,
    body: requestBody,
    headers: defaultHeaders
  }).pipe(
    catchError(error => {
      return of(error);
    })
  );
}

export function logOut() {
  return ajax({
    method: 'POST',
    url: `${apiConfig.baseUrl}logOut`,
    headers: defaultHeaders,
    withCredentials: true
  }).pipe(
    catchError(error => {
      return of(error);
    })
  );
}

export function checkUserName(requestBody) {
  return ajax({
    method: 'POST',
    url: `${apiConfig.baseUrl}usernameCheck`,
    body: requestBody,
    headers: defaultHeaders
  }).pipe(
    catchError(error => {
      return of(error);
    })
  );
}

export function checkUserInHr(requestBody) {
  return ajax({
    method: 'POST',
    url: `${apiConfig.baseUrl}checkFromHr`,
    body: requestBody,
    headers: defaultHeaders
  }).pipe(
    catchError(error => {
      return of(error);
    })
  );
}

export function forgotPassword(requestBody) {
  return ajax({
    method: 'POST',
    url: `${apiConfig.baseUrl}forgotPassword`,
    body: requestBody,
    headers: defaultHeaders
  }).pipe(
    catchError(error => {
      return of(error);
    })
  );
}

export function resetPassword(requestBody) {
  return ajax({
    method: 'POST',
    url: `${apiConfig.baseUrl}passwordReset`,
    body: requestBody,
    headers: defaultHeaders
  }).pipe(
    catchError(error => {
      return of(error);
    })
  );
}

export function changePassword(requestBody) {
  return ajax({
    method: 'PUT',
    url: `${apiConfig.baseUrl}passwordChange`,
    body: requestBody,
    headers: defaultHeaders,
    withCredentials: true
  }).pipe(
    catchError(error => {
      return of(error);
    })
  );
}

export function checkHash(requestBody) {
  return ajax({
    method: 'POST',
    url: `${apiConfig.baseUrl}hashCheck`,
    body: requestBody,
    headers: defaultHeaders
  }).pipe(
    catchError(error => {
      return of(error);
    })
  );
}
