import { of } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { catchError } from 'rxjs/operators';
import apiConfig from 'config/apiConfig';

export function getApplicationList() {
  return ajax({
    method: 'GET',
    url: `${apiConfig.baseUrl}applicationList`,
    headers: {
      'Content-Type': 'application/json'
    },
    withCredentials: true
  }).pipe(
    catchError(error => {
      return of(error);
    })
  );
}
