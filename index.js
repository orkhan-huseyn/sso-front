require('dotenv').config();
const express = require('express');
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');

const app = express();
const { HOST, PORT } = require('./server/config');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'client', 'build')));

require('./server/logSetup')(app);
require('./server/corsSetup')(app);
require('./server/api/v1')(app);

app.get('*', function(_, res) {
  res.sendFile(path.join(__dirname, 'client', 'build/index.html'));
});

app.set('port', PORT);
const server = http.createServer(app);
server.listen(PORT, HOST, function() {
  console.log(`App running on ${HOST}:${PORT}`);
});
