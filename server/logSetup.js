const path = require('path');
const morgan = require('morgan');
const rfs = require('rotating-file-stream');

const accessLogStream = rfs('access.log', {
  interval: '1d',
  path: path.join(__dirname, 'log')
});

module.exports = function(app) {
  app.use(
    morgan(
      ':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length]',
      {
        stream: accessLogStream
      }
    )
  );
};
