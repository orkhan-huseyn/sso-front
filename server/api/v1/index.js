const jwtDecode = require('jwt-decode');
const axios = require('axios');

const { BACKEND_URL, COOKIE_KEY } = require('../../config');
const { handleApiError, getDefaultHeaders } = require('../helpers');

module.exports = function(app) {
  // SSO client  requests for session id
  // then we resposd with our cookies if there is one :)
  app.post('/api/v1/getSID', (req, res) => {
    res.send(req.cookies);
  });

  /** kill session by id */
  app.put('/api/v1/sessionKill/:sessionId', function(req, res) {
    const headers = getDefaultHeaders(req, { withCredentials: true });
    const sessionId = req.params.sessionId;
    axios
      .put(`${BACKEND_URL}/loginLog/kill?id=${sessionId}`, null, {
        headers
      })
      .then(response => {
        res.send(response.data);
      })
      .catch(error => {
        handleApiError(res, error);
      });
  });

  /** get user's active sessions  */
  app.get('/api/v1/activeSessions', function(req, res) {
    const headers = getDefaultHeaders(req, { withCredentials: true });
    axios
      .get(`${BACKEND_URL}/loginLog/activeUserSessions/`, {
        headers
      })
      .then(response => {
        res.send(response.data);
      })
      .catch(error => {
        handleApiError(res, error);
      });
  });

  /** get login history of user */
  app.get('/api/v1/loginHistory', function(req, res) {
    const headers = getDefaultHeaders(req, { withCredentials: true });
    const params = req.query;
    axios
      .get(`${BACKEND_URL}/loginLog/userSessions/`, { headers, params })
      .then(response => {
        res.send(response.data);
      })
      .catch(error => {
        handleApiError(res, error);
      });
  });

  app.get('/api/v1/permittedApps', function(req, res) {
    const headers = getDefaultHeaders(req, { withCredentials: true });
    axios
      .get(`${BACKEND_URL}/application/getByUser`, { headers })
      .then(response => {
        res.send(response.data);
      })
      .catch(error => {
        handleApiError(res, error);
      });
  });

  /** get detailed info about current user  */
  app.get('/api/v1/currentUser', function(req, res) {
    const headers = getDefaultHeaders(req, { withCredentials: true });
    axios
      .get(`${BACKEND_URL}/users/details`, { headers })
      .then(response => {
        res.send(response.data);
      })
      .catch(error => {
        handleApiError(res, error);
      });
  });

  /** change password  */
  app.put('/api/v1/passwordChange', function(req, res) {
    const headers = getDefaultHeaders(req, { withCredentials: true });
    axios
      .put(`${BACKEND_URL}/sign/changePassword`, req.body, { headers })
      .then(response => {
        res.send(response.data);
      })
      .catch(error => {
        handleApiError(res, error);
      });
  });

  // check username and hash for password reset
  app.post('/api/v1/hashCheck', function(req, res) {
    const headers = getDefaultHeaders(req);
    const hash = req.body.passwordResetId;
    axios
      .get(`${BACKEND_URL}/sign/checkHash/${hash}`, { headers })
      .then(response => {
        res.send(response.data);
      })
      .catch(error => {
        handleApiError(res, error);
      });
  });

  // check username for password reset
  // if user exists then send password reset Link
  // to his/her email address
  app.post('/api/v1/forgotPassword', function(req, res) {
    const headers = getDefaultHeaders(req);
    axios
      .get(`${BACKEND_URL}/sign/forgotPassword/${req.body.username}`, {
        headers
      })
      .then(response => {
        res.send(response.data);
      })
      .catch(error => {
        handleApiError(res, error);
      });
  });

  // reset password with new password
  app.post('/api/v1/passwordReset', function(req, res) {
    const body = {
      password1: req.body.password,
      password2: req.body.passwordConfirmation,
      hash: req.body.passwordResetId
    };
    const headers = getDefaultHeaders(req);

    axios
      .post(`${BACKEND_URL}/sign/resetPassword`, body, { headers })
      .then(response => {
        res.cookie(COOKIE_KEY, response.data.token);
        res.send(response.data);
      })
      .catch(error => {
        handleApiError(res, error);
      });
  });

  // check if username existst
  // then send user name, id and photo if so
  app.post('/api/v1/usernameCheck', function(req, res) {
    const headers = getDefaultHeaders(req);
    axios
      .get(`${BACKEND_URL}/sign/checkUsername/${req.body.username}`, {
        headers
      })
      .then(response => {
        res.send(response.data);
      })
      .catch(error => {
        handleApiError(res, error);
      });
  });

  // check if pin exists in hr
  // then send id
  app.post('/api/v1/checkFromHr', function(req, res) {
    const headers = getDefaultHeaders(req);
    axios
      .get(`${BACKEND_URL}/sign/checkFromHR/${req.body.username}`, {
        headers
      })
      .then(response => {
        res.send(response.data);
      })
      .catch(error => {
        handleApiError(res, error);
      });
  });

  // sign in request
  app.post('/api/v1/signIn', function(req, res) {
    const body = {
      userName: req.body.username,
      password: req.body.password
    };
    const headers = getDefaultHeaders(req);

    axios
      .post(`${BACKEND_URL}/sign/in`, body, { headers })
      .then(response => {
        res.cookie(COOKIE_KEY, response.data.token);
        res.send(response.data);
      })
      .catch(error => {
        handleApiError(res, error);
      });
  });

  // registration
  app.post('/api/v1/signUp', function(req, res) {
    const body = {
      id: req.body.id,
      pin: req.body.pin,
      username: req.body.username,
      password: req.body.password
    };
    const headers = getDefaultHeaders(req);

    axios
      .post(`${BACKEND_URL}/sign/up`, body, { headers })
      .then(response => {
        res.send(response.data);
      })
      .catch(error => {
        handleApiError(res, error);
      });
  });

  // log out and kill the token
  app.post('/api/v1/logOut', function(req, res) {
    const headers = getDefaultHeaders(req, { withCredentials: true });
    axios
      .get(`${BACKEND_URL}/sign/logout`, { headers })
      .then(response => {
        res.send(response.data);
      })
      .catch(error => {
        handleApiError(res, error);
      });
  });
};
