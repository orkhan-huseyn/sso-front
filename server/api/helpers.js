const { COOKIE_KEY, APP_NAME } = require('../config');

module.exports.handleApiError = function(res, error) {
  res.header('Content-Type', 'application/json');
  res.status(error.response ? error.response.status : 500);
  res.send(JSON.stringify(error.response ? error.response.data : null));
};

module.exports.getDefaultHeaders = function(req, options) {
  let authHeader = {};
  if (options && options.withCredentials) {
    authHeader = {
      Authorization: `Bearer ${req.cookies[COOKIE_KEY]}`
    };
  }
  return {
    Os: req.headers['os'],
    Browser: req.headers['browser'],
    Ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
    AppName: APP_NAME,
    ...authHeader
  };
};
