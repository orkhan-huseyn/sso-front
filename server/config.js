module.exports.HOST = process.env.HOST;
module.exports.PORT = process.env.PORT;
module.exports.BACKEND_URL = process.env.BACKEND_URL;
module.exports.COOKIE_KEY = process.env.COOKIE_KEY;
module.exports.APP_NAME = process.env.APP_NAME;
module.exports.ORIGIN_WHITELIST = [
  'http://localhost:3001',
  'http://192.168.35.57:3001',
  'http://localhost:3000',
  'http://192.168.35.57:3000'
];
