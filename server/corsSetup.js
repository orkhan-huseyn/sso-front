const { ORIGIN_WHITELIST } = require('./config');

module.exports = function(app) {
  app.use(function(req, res, next) {
    const requestOrigin = req.get('origin');
    if (ORIGIN_WHITELIST.includes(requestOrigin)) {
      res.header('Access-Control-Allow-Origin', requestOrigin);
      res.header('Access-Control-Allow-Credentials', 'true');
      res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept, Os, Browser'
      );
      res.header(
        'Access-Control-Allow-Methods',
        'OPTIONS, GET, POST, PUT, DELETE'
      );
    }
    next();
  });
};
