# Mərkəzi İdentifikasiya və Səlahiyyətlər Sisteminin SSO Modulu

## Haqqında

Mərkəzi İdentifikasiya və Səlahiyyətlər Sistemi çoxlu sayda tətbiqlərin web üzərindən inteqrasiya olunması, idarə edilməsi üçün yaradılmışdır. Sistemi OAuth 2-yə bənzər, cookie əsaslı identifikasiya ilə işləyir və bu səbəbdən cross-domain Single Sign On funksionallığı təqdim edir.

## İstifadə olunan texnologiyalar

Sistemin ərsəyə gəlməsi prosesində Java, Spring Boot, Json Web Tokens, React, NodeJS və məlumat bazası olaraq MS SQL Serverdən istifadə olunmuşdur. Sistemin tərtibatı zamanı heç bir UI kitabxanasından istifadə olunmamışdır və dizayn olaraq [Arion – Admin Dashboard & UI Kit Sketch Template](https://themeforest.net/item/arion-admin-dashboard-ui-kit-sketch-template/23432569) şablonundan istifadə olunmuşdur.

İnterfeys Web Accessibility prinsiplərinin bir çoxuna cavab verir və gecə modunu dəstəkləyir.

## Sistemə daxildir

Sistemi istifadə edərək, istifadəçilər qeydiyyatdan keçə, sistemə daxil ola, şifrələrini bərpa edə bilərlər.
İstifadəçi şəxsi kabinetinə daxil olaraq öz şəxsi məlumatlarını, hansı sistemələrə icazəsi olduğunu görə və şifrəsini yeniləyə bilər. İtifadəçi həmçinin, keçmişdəki login tarixçəsini görə və hər hansı bir şübhəli hal olarsa bunu təyin edə bilər və hal hazırda aktiv olan sessiyaları görə və istədiyi sessiyanı bitirə bilər.
